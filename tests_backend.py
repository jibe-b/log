import backend

def test_update_protocol_returns_nothing():
  expected = type(None)
  actual = type(backend.update_protocol())
  assert(expected == actual)

