import log

def test_load_protocol_retourne_un_dictionnaire():
  expected = type(dict())
  actual = type(log.load_protocol())
  assert(expected == actual)

def test_extract_step_description_returns_a_string():
  expected = type(str())
  actual = type(log.extract_step_description("step_1"))
  assert(expected == actual)

def test_extract_step_description_returns_the_step_description():
  step_id = "step_1"

  expected = "Étape consistant à faire…"
  actual = log.extract_step_description(step_id)
  assert(expected == actual)

def test_prompt_for_value_returns_a_dictionnary():
  expected = type(dict())
  actual = type(log.prompt_for_value("step_1"))
  assert(expected == actual)

# def test_store_value_locally_returns_nothing():
#   expected = type(None)
#   actual = type(log.store_value_locally("var", "0"))
#   assert(expected == actual)

"""
La suite des steps doit être prise en compte
"""

"""
Add DVC support
"""

"""
Align to the Common Workflow language spec (with the lab notebook app (log) wrapped as a tool for manip tools
"""
