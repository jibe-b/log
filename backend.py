import json

def update_protocol():
  protocol = {
              "protocol_id": "protocol_1",
	      "sequence_of_steps": ["step_1", "step_2"],
              "steps": { "step_1":
	        {
		 "step_id": "step_1",
		 "follows": "None",
		 "inputs": [
		   {
		     "type": "https://wikidata/Empty_Glass"
		   }
		 ],
		 "outputs": [
		   {
                     "type": "https://wikidata/Glass_filled_with_water"
		    }
		 ],
		 "tools": [
		   {
		     "tool_id": "tool_1"
		   }
		 ],
		 "out_data": { "https://wikidata/Volume":
		   {
		     "associated_with": "$this.liquid",
		     "observable_id": "https://wikidata/Volume",
		     "value": "",
		     "unit": "",
		     "description": ""
		   }
		 },
		 "action": "geste",
		 "custom_description": "\"Todo: %s avec le %s sur le %s afin d'obtenir un %s. Please enter the value of $variable in : $unit\" %(step['action'],  step['tools'][0]['tool_id'], step['inputs'][0]['type'],  step['outputs'][0]['type'])"
		},

		"step_2":
                {
                 "step_id": "step_2",
                 "follows": "step_1",
                 "inputs": [
                   {
                     "type": "https://wikidata/Empty_Glass"
                   }
                 ],
                 "outputs": [
                   {
                     "type": "https://wikidata/Glass_filled_with_water"
                    }
                 ],
                 "tools": [
                   {
                     "tool_id": "double_decimetre"
                   }
                 ],
                 "out_data": { "https://wikidata/Diameter":
                   {
                     "associated_with": "$this.liquid",
                     "observable_id": "https://wikidata/Diameter",
                     "value": "",
                     "unit": "",
                     "description": ""
                   }
                 },
                 "action": "Second geste",
                }
	      },




               
	      "description": "Ce protocol consiste à…", 
	     }
  json.dump(protocol, open("protocols/protocol_1.json", 'w'))
  pass

