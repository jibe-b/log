#! /home/u/anaconda3/bin/python3

import json
from datetime import datetime

def load_protocol():
  return json.loads(open("protocols/protocol_1.json", 'r').read())

def extract_step_description(step_id):
  protocol = load_protocol()
  for this_step_id in list(protocol["steps"].keys()):
    step = protocol["steps"][this_step_id]
    if this_step_id == step_id:
      if "custom_description" in step.keys():
        description = step["custom_description"]
      else:
        description = "\"%s avec le %s sur le %s afin d'obtenir un %s. Please enter the value of \" %(step['action'],  step['tools'][0]['tool_id'], step['inputs'][0]['type'],  step['outputs'][0]['type'])"
  description_evaluated = eval(description)
  return description_evaluated

def prompt_for_object_id():
  object_id = input("please enter the id of the object")
  return object_id

def prompt_for_value(step_id):
  protocol = load_protocol()
  for this_step_id in protocol["steps"]:
    step = protocol["steps"][this_step_id]
    if this_step_id == step_id:
      variables = list(step["out_data"].keys())

  try:
    value = input("value of " + variables[0] +"?")
  except:
    value = ""
  return {object_id :
                    {variables[0]:
		          { "variable": variables[0],
			    "value": value,
			    "unit": "1",
			    "provenance_step": step_id,
			    "provenance_step_md5": "uiedvuleuid789UULEDVUI7U789",
		            "timestamp": datetime.strftime(datetime.now(), '%Y-%m.%dT%H:%M:%S')
			  }
                    } 
         }

def store_value_locally(data_dict):
  try:
    data_before = json.loads(open("data/data.json", 'r').read())
  except:
    data_before = {}
  
  data = data_before
  object = list(data_dict.keys())[0]
  if object not in data.keys():
    data[object] = {}
  variable = list(data_dict[object].keys())[0]
  if variable not in data[object].keys():
    data[object][variable] = []
  data[object][variable].append({
                              "variable": variable,
			      "value": data_dict[object][variable]["value"],
			      "unit": data_dict[object][variable]["unit"],
			      "provenance_step": data_dict[object][variable]["provenance_step"],
			      "provenance_step_md5": data_dict[object][variable]["provenance_step_md5"],
			      "timestamp": data_dict[object][variable]["timestamp"]
			     })

  json.dump(data, open("data/data.json", 'w'))

if __name__ == "__main__":
  protocol = load_protocol()
  print("starting to execute " + protocol["protocol_id"])

  object_id = prompt_for_object_id()
  for step_id in list(protocol["steps"].keys()):
    description = extract_step_description(step_id)
    print(description)
    data_from_prompt = prompt_for_value(step_id)
    store_value_locally(data_from_prompt)
